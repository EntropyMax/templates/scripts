# The default question JSON keys
answer_key = "answer"
attempts_key = "attempts"
choices_key = "choices"
created_on_key = "created_on"
description_key = "description"
dependencies_key = "dependencies"
disabled_key = "disabled"
etc_key = "estimated_time_to_complete"
explanation_key = "explanation"
failures_key = "failures"
id_key = "_id"
knowledge_key = "knowledge"
ksats_key = "KSATs"
ksats_id_key = "ksat_id"
lsn_objs_key = "lesson_objectives"
libraries_key = "libraries"
group_key = "group"
group_name_key = "group_name"
group_questions_key = "questions"
msbs_key = "MSBs"
passes_key = "passes"
performance_key = "performance"
proficiency_key = "proficiency"
provisioned_key = "provisioned"
question_key = "question"
question_name_key = "question_name"
question_path_key = "question_path"
question_prof_key = "question_proficiency"
question_type_key = "question_type"
rellink_id_key = "rel-link_id"
req_software_key = "req_software"
rev_date_key = "revision_date"
rev_num_key = "revision_number"
snippet_key = "snippet"
snippet_lang_key = "snippet_lang"
solution_key = "solution"
support_files_key = "support_files"
tests_key = "tests"
topic_key = "topic"
topic_path_key = "topic_path"
updated_on_key = "updated_on"
workroles_key = "work-roles"

# The default lesson JSON keys (unique from question JSON keys)
course_key = "course"
demo_lab_key = "demo_lab"
lab_path_key = "lab_path"
labs_key = "labs"
lesson_files = "lesson_files"
lesson_key = "lesson"
module_key = "module"
objectives_key = "objectives"
perf_lab_key = "perf_lab"
software_ver_key = "software_ver"
subject_key = "subject"
trng_topic_key = "Topic"
units_key = "units"

# Question object key
filepath_key = 'filepath'
headerpath_key = 'headerpath'

# The newline characters required to keep multi-line text in CSV files in the same cell
newline_map = "\r\n"

# The standardized name for the output README.md file
readme = "README.md"

# To facilitate printing error and warning text with different colors
error_str = "ERROR"  # Should be formatted for red
warning_str = "WARNING"  # Should be formatted for yellow

# The default usage statement parameter names and descriptions used across all modules
keyword_default = ".question.json"
keyword_desc = "A filter to specify the returned file paths for processing. " \
               "The default is '{0}'.".format(keyword_default)
opt_param_changed_desc = "Gets questions that have changed since the last commit. Without this flag, all JSON file's " \
                         "are retrieved."
param_question_dir_desc = "This is the name of the folder(s) where the '*{0}' files are stored and needs to be " \
                          "relative to the value specified in 'root_dir'. You may specify more than one folder using " \
                          "space delimited values. By default the system collects a list of directories inside the " \
                          "specified 'root_dir' parameter.".format(keyword_default)
param_root_dir_default = "."
param_root_dir_desc = "This is the URL to the target GitLab repository (note, this may be relative). " \
                      "The default is '{}'.".format(param_root_dir_default)
param_md_slide_dirs = "This is the name of the folder(s) where the MD books or slides are stored."
