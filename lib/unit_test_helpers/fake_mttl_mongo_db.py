import json
import os
import sys
import shutil
import pymongo
from bson import json_util
sys.path.append("scripts")
from lib.unit_test_helpers import fake_mttl_data


class FakeMttlDb:
    HOST = os.getenv("MONGO_HOST", "localhost")
    PORT = int(os.getenv("MONGO_PORT", "27017"))

    roadmap_file = "Roadmap.json"
    wkrole_name_map = {
        "PO": "Basic-PO",
        "SPO": "Senior-PO",
        "MPO": "Master-PO",
        "SM": "Scrum-Master",
        "SSM": "Senior-Scrum-Master",
        "MSM": "Master-Scrum-Master",
        "CCD": "Basic-Dev",
        "SCCD-L": "Senior-Dev-Linux",
        "SCCD-W": "Senior-Dev-Windows",
        "MCCD": "Master-Cyber-Capability-Dev",
        "VR": "Vulnerability-Research",
        "Mobile": "Mobile",
        "Networking": "Networking",
        "Sysadmin": "Sysadmin",
        "TAE": "Test-Automation-Engineer",
        "SEE": "Basic-SEE",
        "Instructor": "Instructor"
    }

    rdmap_data = fake_mttl_data.rdmap_data
    rel_link_data = fake_mttl_data.rel_link_data
    requirement_data = fake_mttl_data.requirement_data
    work_role_data = fake_mttl_data.work_role_data

    def __init__(self, folder: str, mongo_host: str = HOST, mongo_port: int = PORT):
        self.client = None
        self.db = None
        self.rel_links = None
        self.reqs = None
        self.wkroles = None
        self.temp_folder = folder
        self.create_rdmap_file()
        self.build_db(mongo_host, mongo_port)

    def create_rdmap_file(self):
        if not os.path.exists(self.temp_folder):
            os.mkdir(self.temp_folder)
        with open(os.path.join(self.temp_folder, self.roadmap_file), "w") as rdmap_fp:
            json.dump(self.rdmap_data, rdmap_fp, indent=4)

    def build_db(self, mongo_host: str = HOST, mongo_port: int = PORT):
        self.client = pymongo.MongoClient(mongo_host, mongo_port)
        self.db = self.client["mttl_unit_test"]
        self.rel_links = self.db["rel_links"]
        self.reqs = self.db["requirements"]
        self.wkroles = self.db["work_roles"]
        self.clear_db()
        self.fill_collections()

    def fill_collections(self):
        # Need to convert the dictionaries into a JSON string so we can convert the $oid into an ObjectID object
        # The work-roles do not use $oid so it doesn't need conversion
        requirement_data = [json_util.loads(json.dumps(item)) for item in self.requirement_data]
        rel_link_data = [json_util.loads(json.dumps(item)) for item in self.rel_link_data]
        self.reqs.insert_many(requirement_data)
        self.wkroles.insert_many(self.work_role_data)
        self.rel_links.insert_many(rel_link_data)

    def remove_files(self):
        if os.path.exists(self.temp_folder):
            shutil.rmtree(self.temp_folder)

    def clear_db(self):
        self.reqs.drop()
        self.wkroles.drop()
        self.rel_links.drop()

    def delete_db(self):
        self.remove_files()
        self.clear_db()
        del self.db
        del self.client
