import sys
import os
import json
import shutil
import unittest
sys.path.append("scripts")
import validate_qids


class ValidateQuestionIdsTests(unittest.TestCase):
    test_dir = "test_validate_question_ids"

    knowl_1 = {
        "_id": "BD_ALG_0001",
        "question_name": "algorithms-big-O.question.json",
        "question_path": "knowledge/Algorithms/algorithms-big-O",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "In computer science, what term is used for the notation to classify algorithms according to how "
                    "their running time or space requirements grow as the input size grows?",
        "choices": [
            "Capacity measure",
            "Big O notation",
            "Complexity rating",
            "Weighted measure"
        ],
        "answer": 1,
        "explanation": [
            "Big-O is the common term used to classify a sorting or searching algorithm according to the size of the "
            "data structure being used."
        ]
    }

    knowl_2 = {
        "_id": "BD_PY_0105",
        "question_name": "doubly-linked-list.question.json",
        "question_path": "knowledge/Data_Structures/data-structures-doubly-linked-lists",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "The above C struct definition is ideal for what type of Data Structure?",
        "snippet": " 1  |  struct Node {\n 2  |    int data;\n 3  |    struct Node *next;\n "
                   "4  |    struct Node *prev;\n 5  |  };\n",
        "snippet_lang": "mermaid",
        "choices": [
            "Linked List",
            "Doubly Linked List",
            "Binary Tree",
            "Graph"
        ],
        "answer": 1,
        "explanation": [
            "Incorrect: Linked list nodes will only have a single pointer to the next node.",
            "Correct: Doubly linked lists will have a pointer that points to the previous node or NULL, and to the "
            "next node or NULL",
            "Incorrect: Although Binary Tree nodes contain two pointers, these typically are labled left and right "
            "respectively",
            "Incorrect: Each node in a graph can point to more than two other graph nodes. Therefore there must be a "
            "way to track an unknown number of nodes such as with an array."
        ]
    }
    knowl_3 = {
        "_id": "BD_C_0115",
        "question_name": "c-basic-file-handling.question.json",
        "question_path": "knowledge/C_Programming/c-basic-file-handling/",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "Which of the following is true when reading and writing files in C?",
        "snippet_lang": "language",
        "choices": [
            "File operations are all functions called with a File object using the dot (.) operator",
            "You can only open in either read or write mode, not both",
            "You have to explicitly close files that are opened",
            "You can only write one character at a time"
        ],
        "answer": 2,
        "explanation": [
            "Incorrect. Objects do not exist in C. The file IO operations in stdio.h are functions that you pass "
            "file names or file descriptors to for them to affect.",
            "Incorrect. You can pass r+ or a+ to open a file in read and write mode.",
            "Correct. File descriptors do not close automatically and unclosed files can use up the file "
            "descriptor table.",
            "Incorrect. You can use fputs() to write character arrays to files."
        ]
    }
    knowl_4 = {
        "_id": "BD_ASM_0002",
        "question_name": "assembly-arithmetic-instructions_1.question.json",
        "question_path": "knowledge/Assembly/assembly-arithmetic-instructions_1/",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "In the above Assembly code, what does eax contain after all the operations?",
        "snippet": "1 | mov eax, 12\n2 | mov ebx, 4\n3 | add eax, ebx\n4 | inc ebx\n5 | inc eax\n6 | shr eax, 1\n"
                   "7 | ",
        "choices": [
            "17",
            "16",
            "8",
            "18"
        ],
        "answer": 2,
        "explanation": [
            "Line 1 moves the value 12 into the eax register and line 2 moves the value 4 into the ebx register. "
            "Line 3 adds the two values, storing the result (16) in the eax register. Line 4 can be ignored since "
            "the ebx register isn't used anymore. Line 5 increments the eax register value by one, now (17), and "
            "line 6 moves bit values right one spot, from 0001 0001 (17) to 0000 1000 (8)."
        ]
    }
    knowl_5 = {
        "_id": "BD_PY_0105",
        "question_name": "changes.question.json",
        "question_path": "knowledge/Git/git-changes",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "What does the 'git diff' command do?",
        "snippet": "",
        "snippet_lang": "language",
        "choices": [
            "Shows unstaged changes between your branch and working directory.",
            "Shows unstaged changes between your branch and previous version of branch.",
            "Shows changes since last commit.",
            "Shows changes between different master branch versions."
        ],
        "answer": 0,
        "explanation": [
            "The git diff command shows unstaged changes between your branch and working directory."
        ]
    }
    knowl_6 = {
        "_id": "BD_PY_0105",
        "question_name": "inheritence.question.json",
        "question_path": "knowledge/Python/inheritence/",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "What is being implemented on line 8?",
        "snippet": "1 |   class Person:\n2 |     def __init__(self, name):\n3 |         self.name = name\n4 |\n"
                   "5 |     def print(self):\n6 |         print(self.name)\n7 |\n8 |   class Airman(Person):\n"
                   "9 |     def __init__(self, name, rank):\n10 |         super().__init__(name)\n"
                   "11 |         self.rank = rank\n12 |\n13 |     def print(self):\n"
                   "14 |         print(self.rank, self.name)  \n15 |\n"
                   "16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}\n17 |\n"
                   "18 |   for peep in people:\n19 |     peep.print()",
        "snippet_lang": "python",
        "choices": [
            "Polymorphism",
            "Inheritance",
            "A list",
            "Overloading"
        ],
        "answer": 1,
        "explanation": [
            "The Airman class is inheriting from the Person class with this syntax  Airman(Person)"
        ],
        "group": {
            "group_name": "python-group_2",
            "questions": [
                "BD_PY_0103",
                "BD_PY_0104",
                "BD_PY_0105",
                "BD_PY_0106",
                "BD_PY_0107",
                "BD_PY_0108",
                "BD_PY_0109"
            ]
        }
    }
    perf_1 = {
        "_id": "BD_C_0067",
        "question_name": "c_binary_search_tree.question.json",
        "question_path": "performance/C_Programming_Converted/c_binary_search_tree/",
        "question_type": "performance_dev",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": {
            "filepath": "performance/C_Programming/c_binary_search_tree/TestCode.c",
            "headerpath": "performance/C_Programming/c_binary_search_tree/TestCode.h"
        },
        "support_files": [],
        "tests": [
            {
                "filepath": "performance/C_Programming/c_binary_search_tree/testcases.cpp"
            }
        ],
        "solution": "performance/C_Programming/c_binary_search_tree/Solution/TestCode.c",
        "dependencies": {
            "libraries": [
                "gmock-1.7.0"
            ],
            "req_software": [
                "MS Visual Studio 2017"
            ]
        }
    }
    perf_2 = {
        "_id": "BD_C_0067",
        "question_name": "invalid_header.question.json",
        "question_path": "performance/C_Programming_Converted/invalid_header/",
        "question_type": "performance_dev",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": {
            "filepath": "performance/C_Programming/c_binary_search_tree/TestCode.c",
            "headerpath": "performance/C_Programming/c_binary_search_tree/TestCode.h"
        },
        "support_files": [],
        "tests": [
            {
                "filepath": "performance/C_Programming/c_binary_search_tree/testcases.cpp",
                "headerpath": ""
            }
        ],
        "solution": "performance/C_Programming/c_binary_search_tree/Solution/TestCode.c",
        "dependencies": {
            "libraries": [
                "gmock-1.7.0"
            ],
            "req_software": [
                "MS Visual Studio 2017"
            ]
        }
    }

    @classmethod
    def build_files(cls):
        mode = "w"
        qpath_key = "question_path"
        qname_key = "question_name"
        if not os.path.exists(cls.test_dir):
            os.mkdir(cls.test_dir)

        # Questions with unique ID usage
        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_1[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_1[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.knowl_1[qpath_key], cls.knowl_1[qname_key]), mode) as knowl_1_fp:
            json.dump(cls.knowl_1, knowl_1_fp)

        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_2[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_2[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.knowl_2[qpath_key], cls.knowl_2[qname_key]), mode) as knowl_2_fp:
            json.dump(cls.knowl_2, knowl_2_fp)

        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_3[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_3[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.knowl_3[qpath_key], cls.knowl_3[qname_key]), mode) as knowl_3_fp:
            json.dump(cls.knowl_3, knowl_3_fp)

        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_4[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_4[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.knowl_4[qpath_key], cls.knowl_4[qname_key]), mode) as knowl_4_fp:
            json.dump(cls.knowl_4, knowl_4_fp)

        if not os.path.exists(os.path.join(cls.test_dir, cls.perf_1[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.perf_1[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.perf_1[qpath_key], cls.perf_1[qname_key]), mode) as perf_1_fp:
            json.dump(cls.perf_1, perf_1_fp)

        # Purposely duplicated question IDs
        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_5[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_5[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.knowl_5[qpath_key], cls.knowl_5[qname_key]), mode) as knowl_5_fp:
            json.dump(cls.knowl_5, knowl_5_fp)

        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_6[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_6[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.knowl_6[qpath_key], cls.knowl_6[qname_key]), mode) as knowl_6_fp:
            json.dump(cls.knowl_6, knowl_6_fp)

        if not os.path.exists(os.path.join(cls.test_dir, cls.perf_2[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.perf_2[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.perf_2[qpath_key], cls.perf_2[qname_key]), mode) as perf_2_fp:
            json.dump(cls.perf_2, perf_2_fp)

    @classmethod
    def remove_files(cls):
        if os.path.exists(cls.test_dir):
            shutil.rmtree(cls.test_dir)

    @classmethod
    def setUpClass(cls) -> None:
        cls.build_files()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.remove_files()

    def setUp(self) -> None:
        self.qpath_key = "question_path"
        self.qname_key = "question_name"
        self.qid_key = "_id"
        self.unique_list = [
            os.path.join(self.test_dir, self.knowl_1[self.qpath_key], self.knowl_1[self.qname_key]).replace("\\", "/"),
            os.path.join(self.test_dir, self.knowl_2[self.qpath_key], self.knowl_2[self.qname_key]).replace("\\", "/"),
            os.path.join(self.test_dir, self.knowl_3[self.qpath_key], self.knowl_3[self.qname_key]).replace("\\", "/"),
            os.path.join(self.test_dir, self.knowl_4[self.qpath_key], self.knowl_4[self.qname_key]).replace("\\", "/"),
            os.path.join(self.test_dir, self.perf_1[self.qpath_key], self.perf_1[self.qname_key]).replace("\\", "/")
        ]
        self.entire_list = self.unique_list + [
            os.path.join(self.test_dir, self.knowl_5[self.qpath_key], self.knowl_5[self.qname_key]).replace("\\", "/"),
            os.path.join(self.test_dir, self.knowl_6[self.qpath_key], self.knowl_6[self.qname_key]).replace("\\", "/"),
            os.path.join(self.test_dir, self.perf_2[self.qpath_key], self.perf_2[self.qname_key]).replace("\\", "/")
        ]

    def test_get_id_map_empty_list(self):
        expected = {}
        self.assertDictEqual(expected, validate_qids.get_id_map([]),
                             msg="Mismatch in returned get_id_map with empty list input")

    def test_get_id_map_unique_list(self):
        expected = {
            self.knowl_1[self.qid_key]: [os.path.join(self.test_dir, self.knowl_1[self.qpath_key],
                                                      self.knowl_1[self.qname_key]).replace("\\", "/")],
            self.knowl_2[self.qid_key]: [os.path.join(self.test_dir, self.knowl_2[self.qpath_key],
                                                      self.knowl_2[self.qname_key]).replace("\\", "/")],
            self.knowl_3[self.qid_key]: [os.path.join(self.test_dir, self.knowl_3[self.qpath_key],
                                                      self.knowl_3[self.qname_key]).replace("\\", "/")],
            self.knowl_4[self.qid_key]: [os.path.join(self.test_dir, self.knowl_4[self.qpath_key],
                                                      self.knowl_4[self.qname_key]).replace("\\", "/")],
            self.perf_1[self.qid_key]: [os.path.join(self.test_dir, self.perf_1[self.qpath_key],
                                                     self.perf_1[self.qname_key]).replace("\\", "/")]
        }
        self.assertDictEqual(expected, validate_qids.get_id_map(self.unique_list),
                             msg="Mismatch in returned get_id_map with unique list input")

    def test_get_id_map_entire_list(self):
        expected = {
            self.knowl_1[self.qid_key]: [os.path.join(self.test_dir, self.knowl_1[self.qpath_key],
                                                      self.knowl_1[self.qname_key]).replace("\\", "/")],
            self.knowl_2[self.qid_key]: [
                os.path.join(self.test_dir, self.knowl_2[self.qpath_key],
                             self.knowl_2[self.qname_key]).replace("\\", "/"),
                os.path.join(self.test_dir, self.knowl_5[self.qpath_key],
                             self.knowl_5[self.qname_key]).replace("\\", "/"),
                os.path.join(self.test_dir, self.knowl_6[self.qpath_key],
                             self.knowl_6[self.qname_key]).replace("\\", "/")
            ],
            self.knowl_3[self.qid_key]: [os.path.join(self.test_dir, self.knowl_3[self.qpath_key],
                                                      self.knowl_3[self.qname_key]).replace("\\", "/")],
            self.knowl_4[self.qid_key]: [os.path.join(self.test_dir, self.knowl_4[self.qpath_key],
                                                      self.knowl_4[self.qname_key]).replace("\\", "/")],
            self.perf_1[self.qid_key]: [
                os.path.join(self.test_dir, self.perf_1[self.qpath_key],
                             self.perf_1[self.qname_key]).replace("\\", "/"),
                os.path.join(self.test_dir, self.perf_2[self.qpath_key],
                             self.perf_2[self.qname_key]).replace("\\", "/")
            ]
        }
        self.assertDictEqual(expected, validate_qids.get_id_map(self.entire_list),
                             msg="Mismatch in returned get_id_map with entire list input")

    def test_get_duplicate_ids_empty_input(self):
        expected = {}
        self.assertDictEqual(expected, validate_qids.check_for_duplicate_ids({}),
                             msg="Mismatch in returned check_for_duplicate_ids with empty dictionary input")

    def test_get_duplicate_ids_unique_input(self):
        id_map = validate_qids.get_id_map(self.unique_list)
        expected = {}
        self.assertDictEqual(expected, validate_qids.check_for_duplicate_ids(id_map),
                             msg="Mismatch in returned check_for_duplicate_ids with unique ID input")

    def test_get_duplicate_ids_entire_input(self):
        id_map = validate_qids.get_id_map(self.entire_list)
        expected = {
            self.knowl_2[self.qid_key]: [
                os.path.join(self.test_dir, self.knowl_2[self.qpath_key],
                             self.knowl_2[self.qname_key]).replace("\\", "/"),
                os.path.join(self.test_dir, self.knowl_5[self.qpath_key],
                             self.knowl_5[self.qname_key]).replace("\\", "/"),
                os.path.join(self.test_dir, self.knowl_6[self.qpath_key],
                             self.knowl_6[self.qname_key]).replace("\\", "/")
            ],
            self.perf_1[self.qid_key]: [
                os.path.join(self.test_dir, self.perf_1[self.qpath_key],
                             self.perf_1[self.qname_key]).replace("\\", "/"),
                os.path.join(self.test_dir, self.perf_2[self.qpath_key],
                             self.perf_2[self.qname_key]).replace("\\", "/")
            ]
        }
        self.assertDictEqual(expected, validate_qids.check_for_duplicate_ids(id_map),
                             msg="Mismatch in returned check_for_duplicate_ids with duplicate ID input")


if __name__ == "__main__":
    unittest.main()
