import sys
import unittest
sys.path.append("scripts")
from lib import csv_helpers


class CsvEscapeSspecialCharsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.str_statement = "This is a statement without punctuation"
        self.str_sentence1 = "This sentence includes punctuation; such as, commas, semi-colons, periods, and so forth."
        self.str_snippet = "1 | import argparse\n2 | \n3 | parser = argparse.ArgumentParser(\"Some Description\")\n" \
                           "4 | \n"

    def test_lit_newline(self):
        expected = self.str_snippet.replace("\n", "\\n").replace("\"", "\"\"")
        self.assertEqual(expected, csv_helpers.csv_escape_special_chars(self.str_snippet), "Mismatch in returned value")

    def test_newline_in_cell(self):
        expected = self.str_snippet.replace("\n", "\r\n").replace("\"", "\"\"")
        self.assertEqual(expected, csv_helpers.csv_escape_special_chars(self.str_snippet, False),
                         "Mismatch in returned value")

    def test_statement(self):
        self.assertEqual(self.str_statement, csv_helpers.csv_escape_special_chars(self.str_statement),
                         "Mismatch in returned value")

    def test_sentence1(self):
        self.assertEqual(self.str_sentence1, csv_helpers.csv_escape_special_chars(self.str_sentence1),
                         "Mismatch in returned value")


class StringifyListForCellTests(unittest.TestCase):
    def setUp(self) -> None:
        self.lst1 = [
            "A is wrong because, as one would say, it's wrong",
            "In order for this to be correct, \nyou'd have to multiply instead of divide.",
            "Correct: this is the \"right\" answer",
            "Nope; you suck again."
        ]
        self.lst2 = [
            "Barny went down the street, riding his bike, to go to the corner store."
        ]
        self.lst3 = []

    def test_empty_list(self):
        self.assertEqual("", csv_helpers.stringify_list_for_cell(self.lst3), "Mismatch in returned value")

    def test_single_item_list(self):
        self.assertEqual(csv_helpers.csv_escape_special_chars("\r\n".join(self.lst2)),
                         csv_helpers.stringify_list_for_cell(self.lst2), "Mismatch in returned value")

    def test_multi_item_list_true(self):
        expected = "\r\n".join([csv_helpers.csv_escape_special_chars(x, True) for x in self.lst1])
        self.assertEqual(expected, csv_helpers.stringify_list_for_cell(self.lst1, True), "Mismatch in returned value")

    def test_multi_item_list_false(self):
        expected = "\r\n".join([csv_helpers.csv_escape_special_chars(x, False) for x in self.lst1])
        self.assertEqual(expected, csv_helpers.stringify_list_for_cell(self.lst1, False), "Mismatch in returned value")


class StringifyListForColsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.lst1 = ["I don't remember this concept being taught"]
        self.lst2 = [
            "I was taught a basic understanding of this concept",
            "I was taught a solid understanding of this concept",
            "I was taught an excellent understanding of this concept",
            "After this training, I feel like I could teach this concept to others"
        ]
        self.lst3 = [
            "After this training, I would need help with this concept\n in practice",
            "\nAfter this training, I can do some of this concept in practice",
            "After this training\nI can do most of this concept in practice",
            "After\nthis training, I can do all of\nthis concept in practice"
        ]
        self.lst4 = []

    def test_empty_list(self):
        self.assertEqual("", csv_helpers.stringify_list_for_cols(self.lst4), "Mismatch in returned value")

    def test_single_item_list(self):
        expected = "\"{0}\"".format("\",\"".join([csv_helpers.csv_escape_special_chars(x) for x in self.lst1]))
        self.assertEqual(expected, csv_helpers.stringify_list_for_cols(self.lst1), "Mismatch in returned value")

    def test_multi_item_list(self):
        expected = "\"{0}\"".format("\",\"".join([csv_helpers.csv_escape_special_chars(x) for x in self.lst2]))
        self.assertEqual(expected, csv_helpers.stringify_list_for_cols(self.lst2), "Mismatch in returned value")

    def test_multi_item_list_newlines(self):
        expected = "\"{0}\"".format("\",\"".join([csv_helpers.csv_escape_special_chars(x) for x in self.lst3]))
        self.assertEqual(expected, csv_helpers.stringify_list_for_cols(self.lst3), "Mismatch in returned value")


class AddCsvContentTests(unittest.TestCase):
    def setUp(self) -> None:
        self.str_statement = "This is a statement without punctuation"
        self.str_sentence1 = "This sentence includes punctuation; such as, commas, semi-colons, periods, and so forth."

    def test_statement(self):
        self.assertEqual(f",{self.str_statement}", csv_helpers.add_csv_content(",", self.str_statement))

    def test_sentence1(self):
        self.assertEqual(f",{self.str_sentence1}", csv_helpers.add_csv_content(",", self.str_sentence1))

    def test_empty_str(self):
        self.assertEqual(f",", csv_helpers.add_csv_content(",", ""))


if __name__ == "__main__":
    unittest.main()
