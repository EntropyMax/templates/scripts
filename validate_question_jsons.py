#!/usr/bin/python3

import os
import json
import sys
import copy
from colorama import Fore
import jsonschema
import argparse
import flatten_dict
from lib.errors import ErrorCode
from lib import file_list_generator
from lib.default_strings import param_root_dir_desc, param_root_dir_default, param_question_dir_desc, \
    opt_param_changed_desc, keyword_default, keyword_desc, question_type_key, error_str, warning_str

avoid_dirs_default = ['.git', '.vscode', 'scripts', 'mttl']
avoid_dirs_desc = f"A list of directories to ignore. The default is '{avoid_dirs_default}'."
dependencies_key = "dependencies"
description = "This function will validate the keys and the values of a json against a provided template. It " \
              "will return True if the json values and keys validate successfully. It will return a list of keys " \
              "that do not match. It will return the key and the reason if the json is not validated."
knowl_schema_default = "knowl_val_schema.json"
knowl_schema_desc = "This is the name of the knowledge question JSON schema validation file; keep in mind, the path " \
                    f"can be relative. The default is '{knowl_schema_default}'."
log_file_default = "ValidateJsonReport.json"
log_file_desc = f"The name of the report file; the default is '{log_file_default}'."
out_dir_default = "."
out_dir_desc = "The directory to store the resulting validation report, relative to root_dir. " \
               f"The default is '{out_dir_default}'."
perf_dev_schema_default = "perf_dev_val_schema.json"
perf_dev_schema_desc = "This is the name of the performance developer JSON schema validation file; keep in mind, the " \
                       f"path can be relative. The default is '{perf_dev_schema_default}'."
perf_nondev_schema_default = "perf_nondev_val_schema.json"
perf_nondev_schema_desc = "This is the name of the performance non-developer JSON schema validation file; keep in " \
                          f"mind, the path can be relative. The default is '{perf_nondev_schema_default}'."
schema_defs_default = "definitions.json"
schema_defs_desc = "This is the name of the schema definitions file used by all JSON schemas. " \
                   f"The default is '{schema_defs_default}'."

msg_er_color = f"{Fore.RED + os.path.basename(__file__)} {error_str + Fore.RESET}:"
msg_er = f"{os.path.basename(__file__)} {error_str}:"
msg_wn_color = f"{Fore.YELLOW + os.path.basename(__file__)} {warning_str + Fore.RESET}:"
msg_wn = f"{os.path.basename(__file__)} {warning_str}:"
msg_gd_color = f"{Fore.GREEN + os.path.basename(__file__) + Fore.RESET}:"
msg_gd = f"{os.path.basename(__file__)}:"


def find_file(keyword: str, start_dir: str) -> str or None:
    '''
    Purpose: This function searches the file system for keyword starting in the start_dir directory. If found, the path
    to the file is returned; otherwise, None is returned.
    :param keyword: The file to search
    :param start_dir: The starting directory of the search
    :return: None = file not found; str = relative path to the file
    '''
    file_path = None
    if not os.path.exists(start_dir):
        # Make sure the start_dir directory exists
        return None
    for (root, dirs, files) in os.walk(start_dir):
        # Recursively search the requested directory for the specified keyword
        for file in files:
            if keyword == os.path.basename(file):
                # In case the file was found, update the location and return the result
                file_path = os.path.relpath(os.path.join(root, file))
                return file_path
    return file_path


def determine_schema_paths(root_dir: str, schema_defs_fname: str, knowl_schema_fname: str, perf_dev_schema_fname: str,
                           perf_nondev_schema_fname:str) -> tuple:
    '''
    Purpose: This function tries to determine the actual location of each schema file.
    :param root_dir: The name of the root or starting directory to search from
    :param schema_defs_fname: The name of the schema definitions file
    :param knowl_schema_fname: The name of the knowledge schema file
    :param perf_dev_schema_fname: The name of the performance developer schema file
    :param perf_nondev_schema_fname: The name of the performance non-developer schema file
    :return: A tuple of each schema file's actual path. If a path is not found it's returned as None
    '''
    # Get everything setup to validate each JSON file; we only want to do this once, not for every file
    defs_schema = None
    knowl_schema = None
    perf_dev_schema = None
    perf_nondev_schema = None

    try:
        if not os.path.exists(schema_defs_fname):
            # If the schema cannot be found, look for its location
            defs_schema = find_file(schema_defs_fname, root_dir)
            if defs_schema is None:
                # Try locating using the CWD instead
                defs_schema = find_file(schema_defs_fname, os.getcwd())
        if not os.path.exists(knowl_schema_fname):
            # If the schema cannot be found, look for its location
            knowl_schema = find_file(knowl_schema_fname, root_dir)
            if knowl_schema is None:
                # Try locating using the CWD instead
                knowl_schema = find_file(knowl_schema_fname, os.getcwd())
        if not os.path.exists(perf_nondev_schema_fname):
            # If the schema cannot be found, look for its location
            perf_nondev_schema = find_file(perf_nondev_schema_fname, root_dir)
            if perf_nondev_schema is None:
                # Try locating using the CWD instead
                perf_nondev_schema = find_file(perf_nondev_schema_fname, os.getcwd())
        if not os.path.exists(perf_dev_schema_fname):
            # If the schema cannot be found, look for its location
            perf_dev_schema = find_file(perf_dev_schema_fname, root_dir)
            if perf_dev_schema is None:
                # Try locating using the CWD instead
                perf_dev_schema = find_file(perf_dev_schema_fname, os.getcwd())
    except (FileNotFoundError, FileExistsError, OSError, IOError):
        # In case the file cannot be found, re-raise the error
        raise

    return defs_schema, knowl_schema, perf_dev_schema, perf_nondev_schema


def build_schemas(schema_defs_path: str, knowl_schema_path: str, perf_dev_schema_path: str,
                  perf_nondev_schema_path: str) -> tuple:
    '''
    Purpose: Adds the defs_schema to the beginning of the parsed schema
    :param schema_defs_path: The path to the schema definitions file
    :param knowl_schema_path: The path to the knowledge question schema file
    :param perf_dev_schema_path: The path to the developer performance question schema
    :param perf_nondev_schema_path: The path to the non-developer performance question schema
    :return: A tuple with each schema represented as a dictionary object
    '''
    mode = "r"  # Only need read access

    with open(schema_defs_path, mode) as defs_fp:
        defs_schema = json.load(defs_fp)
    with open(knowl_schema_path, mode) as knowl_fp:
        knowl_schema = json.load(knowl_fp)
    with open(perf_dev_schema_path, mode) as perf_dev_fp:
        perf_dev_schema = json.load(perf_dev_fp)
    with open(perf_nondev_schema_path, mode) as perf_nondev_fp:
        perf_nondev_schema = json.load(perf_nondev_fp)

    knowledge_schema = {**defs_schema, **knowl_schema}
    performance_dev_schema = {**defs_schema, **perf_dev_schema}
    performance_nondev_schema = {**defs_schema, **perf_nondev_schema}

    return knowledge_schema, performance_dev_schema, performance_nondev_schema


def determine_schema(this_json: dict, knowl_schema: dict, perf_dev_schema: dict, perf_nondev_schema: dict) -> dict:
    '''
    Purpose: This function determines the JSON schema necessary to validate an individual *.rel-link.json.
    :param this_json: The file to be validated.
    :param knowl_schema: The knowledge JSON schema dictionary object.
    :param perf_dev_schema: The performance developer JSON schema  dictionary object.
    :param perf_nondev_schema: The performance JSON schema  dictionary object (not dev).
    :return: parsed_schema - The schema dictionary object that should be used; if {}, then the schema couldn't be
                             determined.
    '''
    parsed_schema = {}  # Discover and assign the correct schema for the current object
    if question_type_key in this_json:
        # In case the expected attribute exists, determine the schema
        if this_json[question_type_key] == "performance_dev":
            # In case this is a performance dev question
            parsed_schema = perf_dev_schema
        elif this_json[question_type_key] == "performance_non-dev":
            # In case this is a performance non-dev question
            parsed_schema = perf_nondev_schema
        elif this_json[question_type_key] == "knowledge":
            # In case this is a knowledge question
            parsed_schema = knowl_schema
    else:
        print(f"{msg_er_color} Unable to determine schema due to a missing 'question_type' key in: '{this_json}'")

    return parsed_schema


def group_check(this_json: dict, schema: dict):
    '''
    Purpose: This determines if the group attribute should be required.
    :param this_json: The current question
    :param schema: The current schema for interrogation/update
    :return: updated_schema - A dictionary representing the updated schema
    '''
    updated_schema = copy.deepcopy(schema)
    for index, schema_part in enumerate(updated_schema["allOf"]):
        if "properties" in schema_part and this_json["question_type"] == "knowledge" and "group" in this_json:
            updated_schema["allOf"][index]["required"].append("group")
    return updated_schema


def snippet_check(this_json: dict, schema: dict):
    '''
    Purpose: This determines if the snippet and snippet_lang attributes should be required.
    :param this_json: The current question
    :param schema: The current schema for interrogation/update
    :return: updated_schema - A dictionary representing the updated schema
    '''
    updated_schema = copy.deepcopy(schema)
    for index, schema_part in enumerate(updated_schema["allOf"]):
        if "properties" in schema_part and this_json["question_type"] == "knowledge" and \
                ("snippet" in this_json or "snippet_lang" in this_json):
            updated_schema["allOf"][index]["required"].append("snippet")
            updated_schema["allOf"][index]["required"].append("snippet_lang")
    return updated_schema


def strip_whitespace(this_json: dict) -> dict:
    '''
    Purpose: This strips leading & trailing whitespace from all strings in the input 'this_json' dictionary object.
    :param this_json: A dictionary representing the question
    :return: A dictionary representing the input question with all leading/trailing whitespace stripped
    '''
    this_json_flat = flatten_dict.flatten(this_json)
    for key, value in this_json_flat.items():
        if isinstance(value, str):
            this_json_flat[key] = value.strip()
        elif isinstance(value, list):
            if all(isinstance(item, dict) for item in value):
                this_json_flat[key] = [strip_whitespace(list_dict) for list_dict in value]
            else:
                this_json_flat[key] = [this_string.strip() for this_string in value if isinstance(this_string, str)]
    return flatten_dict.unflatten(this_json_flat)


def validate_json(this_json: dict, this_file: str, schema: dict) -> dict:
    '''
    Purpose: This function validates a rel-link JSON file. The non-verbose optiononly validates the necessary keys;
    the verbose option also checks some values.
    :param this_json: A dictionary object representing the *.rel-link.json file requiring validation.
    :param this_file: The path of the *.rel-link.json file requiring validation.
    :param schema: A dictionary object representing the JSON schema to use for validation of the *.rel-link.json
                   dictionary object.
    :return: this_status: {} = Success; otherwise, one or more keys representing the error and its message.
    '''
    this_status = {}
    try:
        schema = snippet_check(this_json, schema)  # Require snippet and snippet_lang attributes where applicable
        schema = group_check(this_json, schema)  # Require group attribute where applicable
        this_json = strip_whitespace(this_json)  # Strip leading/trailing whitespace in this_json strings
        jsonschema.validate(instance=this_json, schema=schema)

    except jsonschema.ValidationError as e:
        msg = "{0} in '{1}' key of '{2}'"
        msg_key = "[{0}]".format("][".join([str(x) for x in e.relative_path]))
        msg_details = [
            "Failed validating '{0}': {1}".format(e.validator, e.message),
            {"On instance:": {
                "[{0}]".format("][".join([str(x) for x in e.absolute_path])): e.instance
            }},
            {"In schema": "[{0}]".format("][".join([str(x) for x in e.schema_path]))}
        ]
        print("{0}: {1}".format(
            msg.format(msg_er_color, Fore.RED + msg_key + Fore.RESET, Fore.CYAN + this_file + Fore.RESET),
            msg_details
        ))
        if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
        this_status[ErrorCode.JSON_DECODE_ERROR.value].append({msg.format(msg_er, msg_key, this_file): msg_details})

    return this_status


def main(args: argparse.Namespace) -> dict:
    '''
    Purpose: This function validates all question JSON files in the requested GitLab Repository.
    :return: this_status - a dictionary with keys representing error codes according to class Error or the exception,
             when present.
    '''
    this_status = {}

    if args.question_dirs is None:
        args.question_dirs = ["."]

    try:
        # Get everything setup to validate each JSON file; we only want to do this once, not for every file
        # Determine the actual path to each schema file
        schema_defs, knowl_schema, pf_dev_schema, pf_nondev_schema = determine_schema_paths(args.root_dir,
                                                                                            args.schema_defs_fname,
                                                                                            args.knowl_schema_fname,
                                                                                            args.pf_dev_schema_fname,
                                                                                            args.pf_nondev_schema_fname)

        if schema_defs is None or knowl_schema is None or pf_dev_schema is None or pf_nondev_schema is None:
            msg = "{0} Unable to locate one or more schemas. Please check that the file exists."
            print(msg.format(msg_er_color))
            this_status[ErrorCode.INVALID_INPUT.value] = [msg.format(msg_er)]
            return this_status

        # Now that we know the actual path, each schema needs loaded to a dict obj with the definitions added
        knowl_schema, pf_dev_schema, pf_nondev_schema = build_schemas(schema_defs, knowl_schema, pf_dev_schema,
                                                                      pf_nondev_schema)
        if args.changed:
            # In case only changed files are requested
            paths = file_list_generator.changed_paths(repo_base_dir=args.root_dir, keyword=args.filter,
                                                      avoid_list=args.avoid_dirs)
        else:
            # In case all files are requested
            paths = file_list_generator.dir_walk(root_dir=args.root_dir, json_dirs=args.question_dirs,
                                                 keyword=args.filter, avoid_list=args.avoid_dirs)
        if len(paths) == 0:
            # In case the list is empty
            msg = "{0} Received empty file list."
            print(msg.format(msg_wn_color))
            this_status[ErrorCode.EMPTY_LIST.value] = [msg.format(msg_wn)]
            return this_status
        out_dir = args.out_dir
        if not os.path.exists(out_dir):
            # Make sure the output directory exists to prevent write-to-disk errors
            os.makedirs(out_dir)
    except (json.JSONDecodeError, ValueError) as err:
        msg = "{0} {1}: {2}"
        print(msg.format(msg_er_color, ErrorCode.INVALID_INPUT.name, err))
        this_status[ErrorCode.JSON_DECODE_ERROR.value] = [msg.format(msg_er, ErrorCode.INVALID_INPUT.name, err)]
        return this_status

    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        # In case the file cannot be opened, display a reason and stop processing
        msg = "({0}){1}".format(err.errno, err)
        print("{0} {1}".format(msg_er_color, msg))
        if err.errno not in this_status:
            this_status[err.errno] = []
        this_status[err.errno].append("{0} {1}".format(msg_er, msg))
        return this_status

    else:
        for this_file in paths:
            # Validate the current file against its schema
            try:
                # Determine the schema to be used for the current file
                with open(this_file, "r") as this_json_fp:
                    this_json = json.load(this_json_fp)

                parsed_schema = determine_schema(this_json, knowl_schema, pf_dev_schema, pf_nondev_schema)

                if {} == parsed_schema:
                    msg = "{0} Unable to determine the Schema for validation. Ensure '{1}' uses the proper Format."
                    print(msg.format(msg_er_color, Fore.CYAN + this_file.replace("\\", "/") + Fore.RESET))
                    if ErrorCode.INVALID_INPUT.value not in this_status:
                        this_status[ErrorCode.INVALID_INPUT.value] = []
                    this_status[ErrorCode.INVALID_INPUT.value].append(msg.format(msg_er, this_file.replace("\\", "/")))
                    continue

            except (json.JSONDecodeError, IOError, ValueError, TypeError, OverflowError) as err:
                msg = "{0} while setting up {1}"
                print("{0}: {1}.".format(
                    msg.format(msg_er_color, Fore.RED + this_file.replace("\\", "/") + Fore.RESET),
                    err
                ))
                if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
                    this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
                this_status[ErrorCode.JSON_DECODE_ERROR.value] += [
                    {msg.format(msg_er, this_file.replace("\\", "/")): [str(err)]}
                ]
            else:
                # Now we're ready to perform validation
                log = validate_json(this_json, this_file, parsed_schema)
                for code, err_list in log.items():
                    if code not in this_status:
                        this_status[code] = []
                    this_status[code] += err_list
    try:
        if len(this_status) == 0:
            # In case this_status has no errors or warnings
            msg = "All Jsons are validated."
            print(Fore.GREEN + msg + Fore.RESET)
            if ErrorCode.SUCCESS.value not in this_status:
                this_status[ErrorCode.SUCCESS.value] = []
            this_status[ErrorCode.SUCCESS.value].append(msg)
        elif len(this_status) == 1 and ErrorCode.SUCCESS.value in this_status:
            # In case there are only warnings
            msg = "All JSONs appear to be valid; however, warnings are present. Refer to '{0}' for details."
            print(Fore.YELLOW + msg.format(
                Fore.CYAN + os.path.join(out_dir, args.log_file).replace("\\", "/") + Fore.YELLOW) + Fore.RESET)
            if ErrorCode.SUCCESS.value not in this_status:
                this_status[ErrorCode.SUCCESS.value] = []
            this_status[ErrorCode.SUCCESS.value].append(msg)
        else:
            msg = "{0} {1}: Refer to '{2}' for details."
            print(msg.format(
                msg_er_color,
                Fore.RED + ErrorCode.INVALID_JSON_FORMAT.name + Fore.RESET,
                Fore.CYAN + os.path.join(out_dir, args.log_file).replace("\\", "/") + Fore.RESET
            ))
            if ErrorCode.INVALID_JSON_FORMAT.value not in this_status:
                this_status[ErrorCode.INVALID_JSON_FORMAT.value] = []
            this_status[ErrorCode.INVALID_JSON_FORMAT.value].append(msg.format(
                msg_er,
                ErrorCode.INVALID_JSON_FORMAT.name,
                os.path.join(out_dir, args.log_file).replace("\\", "/")
            ))
        with open(os.path.join(out_dir, args.log_file), "w") as log_fp:
            json.dump(this_status, log_fp, indent=4)
    except (IOError, json.JSONDecodeError, ValueError, TypeError, OverflowError) as err:
        msg = "{0} Could not create the json report file; {1}"
        print(msg.format(msg_er_color, err))
        if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
        this_status[ErrorCode.JSON_DECODE_ERROR.value].append(msg.format(msg_er, err))
        return this_status
    return this_status


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-a", "--avoid_dirs", nargs='+', type=str, default=avoid_dirs_default, help=avoid_dirs_desc)
    parser.add_argument("-c", "--changed", action="store_true", help=opt_param_changed_desc)
    parser.add_argument("-f", "--filter", type=str, default=keyword_default, help=keyword_desc)
    parser.add_argument("-k", "--knowl_schema_fname", type=str, default=knowl_schema_default, help=knowl_schema_desc)
    parser.add_argument("-l", "--log_file", type=str, default=log_file_default, help=log_file_desc)
    parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("-P", "--pf_dev_schema_fname", type=str, default=perf_dev_schema_default,
                        help=perf_dev_schema_desc)
    parser.add_argument("-p", "--pf_nondev_schema_fname", type=str, default=perf_nondev_schema_default,
                        help=perf_nondev_schema_desc)
    parser.add_argument("-q", "--question_dirs", nargs='+', type=str, default=None, help=param_question_dir_desc)
    parser.add_argument("-R", "--root_dir", type=str, default=param_root_dir_default, help=param_root_dir_desc)
    parser.add_argument("-s", "--schema_defs_fname", type=str, default=schema_defs_default, help=schema_defs_desc)
    params = parser.parse_args()
    status = main(params)
    if not (len(status) == 1 and ErrorCode.SUCCESS.value in status):
        # In case status has errors, prevent returning zero
        if ErrorCode.SUCCESS.value in status:
            # For cases where ErrorCode.SUCCESS.value isn't present
            del status[ErrorCode.SUCCESS.value]
    sys.exit(list(status.keys())[0])
